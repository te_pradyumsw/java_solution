package com.tyss.codePratice;

import java.util.Arrays;

public class ArraySorting {

	public static void main(String args []) {
		int arr [] = {1,20,4,10,23,17,5,9}; // array with integer values
		Arrays.sort(arr); //Array is sorted in ascending order
		int b= arr.length; // length of an array
		
		
		
System.out.println("==============================Sorting in Descending Order===========================");
		for(int i=arr.length-1;i<arr.length;i--) {
			if (i<0) {
				break;
			} else {
			System.out.println("Elements after sorting in desc order are "+arr[i]);
			}
		}
		
System.out.println("===============================Sorting in Ascending Order =============================");
	for (int i = 0; i < arr.length; i++) {
		System.out.println("Elements sorting in Ascending order are "+arr[i]);
	}
	
	System.out.println("=============================Maximum value of Array============================================");

	int maximumValue = arr[b-1];
	System.out.println("The maximum value is "+ maximumValue);
	
	System.out.println("=============================2nd Maximum value of Array============================================");
	
	int maximum2ndValue = arr[b-2];
	System.out.println("The second maximum value is "+ maximum2ndValue);
	
	System.out.println("==================== second minimum value of Array=================");
	int minimum2ndValue= arr[1];
	System.out.println("The second minimum value is "+ minimum2ndValue);
	
	
	}
	



}
