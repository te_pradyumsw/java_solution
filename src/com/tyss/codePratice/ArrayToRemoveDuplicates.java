package com.tyss.codePratice;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class ArrayToRemoveDuplicates {

	public static void main(String a[]) {
		int[] arr = new int[] { 10, 9, 3, 5, 7, 2, -1, 9, 8, -1, 2, 3}; // array declaration and values
		
		/**
		 * Logic to Remove duplicate values from Array
		 * LinkedHashSet<Integer> does not allow duplicate values
		 * 
		 */
		System.out.println("=====================Remove Duplicate values===============================");
		LinkedHashSet<Integer> removeDuplicated = new LinkedHashSet<Integer>();
		for (int arrValue : arr) {
			removeDuplicated.add(arrValue);
		}
		System.out.println("Elements after Removing duplicates value" + removeDuplicated);

		System.out.println("=================Print duplicate Values==============================");
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					System.out.println("duplicate values are " + arr[j]);
				}
			}

		}

		System.out.println("===========================count Appreance of a element in a array============================");
		Map<Integer, Integer> countApperances = new HashMap<Integer, Integer>();
		for (int intValue : arr) {
			if (!countApperances.containsKey(intValue)) {

				countApperances.put(intValue, 1);
			} else {
				countApperances.put(intValue, countApperances.get(intValue) + 1);

			}

		}
		System.out.println("Frequency of an Element in a Array is " + countApperances);
	}
}
